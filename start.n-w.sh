#!/usr/bin/env bash
cd "$(dirname "$(readlink -f "$0")")" || exit
echo "Support can be provided on our Matrix channel.

Pain heals, chicks dig scars, Glory lasts forever!

"

source "$PWD/actions.sh"

[ ! -d "$JC_DIRECTORY" ] && mkdir -p "$JC_DIRECTORY"

# terminal
[ $TERMINAL_OUTPUT = 0 ] && exec &>/dev/null

# dwarfs
[ "$EXTRACT" = 0 ] && dwarfs-mount || { dwarfs-extract; UNMOUNT=0; }
[ "$UNMOUNT" = 1 ] && trap jc141-cleanup EXIT INT SIGINT SIGTERM

# wine
export WINEPREFIX="$JC_DIRECTORY/wine-prefix"
export WINEDLLOVERRIDES="winemenubuilder.exe=d;mshtml=d"
export WINE_LARGE_ADDRESS_AWARE=1
export WINE_D3D_CONFIG="renderer=vulkan"
[ ! -d "$WINEPREFIX" ] && wine-initiate_prefix

# launch command
GAMEROOT="$PWD/files/game-root"; CMD=( "$SYSWINE" "game.exe" "$@" )

# do NOT touch!
declare -a RUN

# gamescope
[ -x "$(command -v "gamescope")" ] && [ $GAMESCOPE = 1 ] && RUN+=( gamescope-run_embedded )

# isolation
[ $ISOLATE = 1 ] && { export ISOLATION_TYPE='wine'; RUN+=( bash 'actions.sh' bwrap-run_in_sandbox --chdir "$GAMEROOT" ); } || cd "$GAMEROOT"

# start
RUN+=( "${CMD[@]}" ); "${RUN[@]}"
